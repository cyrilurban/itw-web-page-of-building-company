 /*
    @author  CYRIL URBAN
    @date    2018-04-29
    @brief   ITW2 - project 2. Page for building company
 */

$(document).ready(function(){
	$("#service_text01").show();
	$("#service_image01").show();
	$("#service_text02").hide();
	$("#service_image02").hide();
	$("#service_text03").hide();
	$("#service_image03").hide();

    $("#services01").click(function(){
        $("#service_text01").show();
        $("#service_image01").show();
        $("#service_text02").hide();
        $("#service_image02").hide();
        $("#service_text03").hide();
        $("#service_image03").hide();

        document.getElementById("services01").className = "sub_select";
        document.getElementById("services02").className = "";
        document.getElementById("services03").className = "";
    });

    $("#services02").click(function(){
        $("#service_text01").hide();
        $("#service_image01").hide();
        $("#service_text02").show();
        $("#service_image02").show();
        $("#service_text03").hide();
        $("#service_image03").hide();

        document.getElementById("services01").className = "";
        document.getElementById("services02").className = "sub_select";
        document.getElementById("services03").className = "";
    });

    $("#services03").click(function(){
       	$("#service_text01").hide();
        $("#service_image01").hide();
        $("#service_text02").hide();
        $("#service_image02").hide();
        $("#service_text03").show();
        $("#service_image03").show();

        document.getElementById("services01").className = "";
        document.getElementById("services02").className = "";
        document.getElementById("services03").className = "sub_select";
    });
});